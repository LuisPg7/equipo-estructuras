function tabular(){
	
	var Vp=20000;
	var T=0.12;
	var P=12;
	var Iva;
	var Pi=Vp/P;
	var S=20000;
	var I;
	var A=Pi;
	var Fo=A+(I*(1+Iva));
	var Dp;
	var flujo;

		let productos = new Array(P);
		
		for (var i = 0 ; i < P; i++) {
			Dp=cdias(i+1);
			I=(S)*(Dp)*(T/360);
			Iva=I*0.16;
			flujo=Iva+I+Pi;
			productos[i]={	periodo: i+1,
							inicio: sumames(i+1),
							fin: sumames(i+2),
							dias: cdias(i+1),
							disposicion: "    ",
							insoluto: S,
							amortizacion: Pi,
							intereses: I,
							iva: Iva,
							flujo: flujo};
			S-=Pi;

		}
		

		let tablaProducto= document.getElementById('productos');
		let cuerpoTabla = document.createElement('tbody');

		productos.forEach(p => {
			
			let fila=document.createElement('tr');

			let td =document.createElement('td');
			td.innerText = p.periodo;
			fila.appendChild(td);

			td = document.createElement('td');
			td.innerText = p.inicio;
			fila.appendChild(td);

			td = document.createElement('td');
			td.innerText = p.fin;
			fila.appendChild(td);

			td = document.createElement('td');
			td.innerText = p.dias;
			fila.appendChild(td);

			td = document.createElement('td');
			td.innerText = p.disposicion;
			fila.appendChild(td);

			td = document.createElement('td');
			td.innerText = p.insoluto;
			fila.appendChild(td);

			td = document.createElement('td');
			td.innerText = p.amortizacion;
			fila.appendChild(td);

			td = document.createElement('td');
			td.innerText = p.intereses;
			fila.appendChild(td);

			td = document.createElement('td');
			td.innerText = p.iva;
			fila.appendChild(td);


			td = document.createElement('td');
			td.innerText = p.flujo;
			fila.appendChild(td);

			cuerpoTabla.appendChild(fila);
		});

		tablaProducto.appendChild(cuerpoTabla);
 		}


		
function sumames(i){

			var e = new Date();
			var fecha;
			if (e.getMonth()+i>12){
				fecha = (e.getDate()+"-"+((e.getMonth()+i)-12)+"-"+(e.getFullYear()+1)); 

			}else{
				fecha = (e.getDate()+"-"+(e.getMonth()+i)+"-"+e.getFullYear()); 
			}
			return fecha;
		}

function cdias(i){
			var e = new Date();
			var año, mes;
			if (e.getMonth()+i>12){
				año = e.getFullYear()+1;
				mes = (e.getMonth()+i)-12;
			}else{
				año = e.getFullYear(); 
				mes = e.getMonth()+i;
			}
			return new Date(año,mes,0).getDate(); 
		}
